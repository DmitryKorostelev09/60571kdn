<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('reis/update'); ?>
    <input type="hidden" name="id" value="<?= $reis["id"] ?>">
    <div class="form-group">
        <label for="id_marshryt">Маршрут:</label>
        <select class="form-control <?= ($validation->hasError('id_marshryt')) ? 'is-invalid' : ''; ?>"
                name='id_marshryt'
                onChange="" id="id_marshryt">
            <option value="-1">Выберите маршрут</option>
            <?php foreach ($marshryt as $item): ?>
                <option value="<?= ($item['id']) ?>" <?php if ($reis["id_marshryt"] == $item['id']) echo "selected"; ?>>
                    <?= esc($item['naimen']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_marshryt') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="id_transport">Транспорт:</label>
        <select class="form-control <?= ($validation->hasError('id_transport')) ? 'is-invalid' : ''; ?>"
                name='id_transport'
                onChange="" id="id_transport">
            <option value="-1">Выберите транспорт</option>
            <?php foreach ($transport as $item): ?>
                <option value="<?= ($item['id']) ?>" <?php if ($reis["id_transport"] == $item['id']) echo "selected"; ?>>
                    <?= esc($item['naimenovanie']); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_transport') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date_otpr">Дата и время отправки:</label>
        <label style="width: 100%; display:flex;">
            <input type="date"
                   class="form-control <?= ($validation->hasError('date_otpr')) ? 'is-invalid' : ''; ?>"
                   name="date_otpr"
                   value="<?= $reis["date_otpr"] ?>"
                   id="date_otpr"
                   placeholder="yyyy-mm-dd">
            <input type="time"
                   class="form-control <?= ($validation->hasError('time_otpr')) ? 'is-invalid' : ''; ?>"
                   name="time_otpr"
                   value="<?= $reis["time_otpr"] ?>"
                   placeholder="hh:mm">
        </label>
        <div class="invalid-feedback">
            <?= $validation->getError('date_otpr') ?>
            <?= $validation->getError('time_otpr') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date_prib">Дата и время прибытия:</label>
        <label style="width: 100%; display:flex;">
            <input type="date"
                   class="form-control <?= ($validation->hasError('date_prib')) ? 'is-invalid' : ''; ?>"
                   name="date_prib"
                   value="<?= $reis["date_prib"] ?>"
                   id="date_prib"
                   placeholder="yyyy-mm-dd">
            <input type="time"
                   class="form-control <?= ($validation->hasError('time_prib')) ? 'is-invalid' : ''; ?>"
                   name="time_prib"
                   value="<?= $reis["time_prib"] ?>"
                   placeholder="hh:mm">
        </label>
        <div class="invalid-feedback">
            <?= $validation->getError('date_prib') ?>
            <?= $validation->getError('time_prib') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture_url">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
               name="picture"
               id="picture_url"
               value="<?= $reis["picture_url"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
