<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('reis/store'); ?>
    <div class="form-group">
        <label for="id_marshryt">Маршрут:</label>
        <select class="form-control <?= ($validation->hasError('id_marshryt')) ? 'is-invalid' : ''; ?>"
                name='id_marshryt'
                onChange="" id="id_marshryt">
            <option value="-1">Выберите маршрут</option>
            <?php foreach ($marshryt as $item): ?>
                <option value=<?= esc($item['id']); ?>><?= esc($item['naimen']); ?> </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_marshryt') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="id_transport">Транспорт:</label>
        <select class="form-control <?= ($validation->hasError('id_transport')) ? 'is-invalid' : ''; ?>"
                name='id_transport'
                onChange="" id="id_transport">
            <option value="-1">Выберите транспорт</option>
            <?php foreach ($transport as $item): ?>
                <option value=<?= esc($item['id']); ?>><?= esc($item['naimenovanie']); ?> </option>
            <?php endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_transport') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date_otpr">Дата и время отправки:</label>
        <label style="width: 100%; display:flex;">
            <input type="date"
                   class="form-control <?= ($validation->hasError('date_otpr')) ? 'is-invalid' : ''; ?>"
                   name="date_otpr"
                   value="<?= old('date_otpr'); ?>"
                   id="date_otpr"
                   placeholder="yyyy-mm-dd">
            <input type="time"
                   class="form-control <?= ($validation->hasError('time_otpr')) ? 'is-invalid' : ''; ?>"
                   name="time_otpr"
                   value="<?= old('time_otpr'); ?>"
                   placeholder="hh:mm">
        </label>
        <div class="invalid-feedback">
            <?= $validation->getError('date_otpr') ?>
            <?= $validation->getError('time_otpr') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date_prib">Дата и время прибытия:</label>
        <label style="width: 100%; display:flex;">
            <input type="date"
                   class="form-control <?= ($validation->hasError('date_prib')) ? 'is-invalid' : ''; ?>"
                   name="date_prib"
                   value="<?= old('date_prib'); ?>"
                   id="date_prib"
                   placeholder="yyyy-mm-dd">
            <input type="time"
                   class="form-control <?= ($validation->hasError('time_prib')) ? 'is-invalid' : ''; ?>"
                   name="time_prib"
                   value="<?= old('time_prib'); ?>"
                   placeholder="hh:mm">
        </label>
        <div class="invalid-feedback">
            <?= $validation->getError('date_prib') ?>
            <?= $validation->getError('time_prib') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
               name="picture" id="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
