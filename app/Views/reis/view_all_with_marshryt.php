<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2 class="mb-4 text-center">Маршруты с рейсами</h2>
    <div class="d-flex justify-content-center mb-4">
        <?= $pager->links('group1', 'my_page') ?>
        <?= form_open('reis/viewAllWithMarshryt', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-light" type="submit">На странице</button>
        </form>
        <?= form_open('reis/viewAllWithMarshryt', ['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Город или тариф" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-light" type="submit">Найти</button>
        </form>
    </div>

    <div class="row mb-3" style="text-align: center; align-items:center;">
        <div class="col-3">Маршрут</div>
        <div class="col-2">Тариф за 1 кг</div>
        <div class="col-2">Расстояние</div>
        <div class="col-2 ">Дата и время отправки</div>
        <div class="offset-1 col-2">Дата и время прибытия</div>
    </div>


    <?php if (!empty($reis) && is_array($reis)) : ?>
        <?php foreach ($reis as $item): ?>
            <a href="<?= base_url() ?>/reis/view/<?= esc($item['id']); ?>" class="row mb-3 py-3"
               style="color: black; text-align:center; background: white; border-radius: 5px;">
                <strong class=" col-3"><?= esc($item['naimen']); ?></strong>
                <span class="col-2"><?= esc($item['tarif']); ?> руб.</span>
                <span class="col-2"><?= esc($item['rasstoyanie']); ?> км</span>
                <span class="col-2 "><?= esc($item['date_otpr']); ?> <?= esc($item['time_otpr']); ?></span>
                <span class="offset-1 col-2"><?= esc($item['date_prib']); ?> <?= esc($item['time_prib']); ?></span>
            </a>
        <?php endforeach; ?>
    <?php else : ?>
        <p class="text-center">Невозможно найти маршруты.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
