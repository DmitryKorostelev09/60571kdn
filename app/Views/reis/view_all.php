<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2 class="mb-4 text-center">Все рейсы</h2>
        <div class="row mb-3" style="text-align: center; align-items:center;">
            <div class="col-2">Фото</div>
            <div class="col-4">Маршрут</div>
            <div class="col-2">Транспорт</div>
            <div class="col-2">Дата и время отправки</div>
            <div class="col-2">Дата и время прибытия</div>
            <div class="offset-4"></div>
        </div>
        <?php if (!empty($reis) && is_array($reis)) : ?>
            <?php foreach ($reis as $item): ?>
                <div class="row mb-3 py-3" style="color: black; background: white; align-items: center; border-radius: 5px; text-align:center;">
                    <div class="col-2">
                        <?php if (is_null($item['picture_url'])) : ?>
                            <img src="<?= base_url() ?>/img/fast.svg" class="w-100" alt="">
                        <?php else:?>
                            <img src="<?= esc($item['picture_url']); ?>" class="w-100" alt="">
                        <?php endif ?>
                    </div>
                    <div class="col-4"><?= esc($item['naimen']); ?></div>
                    <div class="col-2"><?= esc($item['naimenovanie']); ?></div>
                    <div class="col-2">
                        <?= esc($item['date_otpr']); ?> <?= esc($item['time_otpr']); ?>
                    </div>
                    <div class="col-2">
                        <?= esc($item['date_prib']); ?> <?= esc($item['time_prib']); ?>
                    </div>
                    <div class="col-12 text-right">
                        <a href="<?= base_url() ?>/reis/view/<?= esc($item['id']); ?>"
                           class="btn btn-primary">Просмотреть</a>
                        <a href="<?= base_url() ?>/reis/edit/<?= esc($item['id']); ?>"
                           class="btn btn-primary">Изменить</a>
                        <a href="<?= base_url() ?>/reis/delete/<?= esc($item['id']); ?>"
                           class="btn btn-primary">Удалить</a>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти маршруты.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>