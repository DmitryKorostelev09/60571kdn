<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($reis)) : ?>
    <div class="card mx-auto" style="max-width: 340px; color: black;">
        <div class="row">
            <div class="col-12">
                <div class="card-body" style="text-align:center;">
                    <p class="card-title">Маршрут: <?= esc($reis['naimen']); ?></p>
                    <p class="card-text">Транспорт: <?= esc($reis['naimenovanie']); ?></p>
                    <p class="card-text">
                        <span class="d-block">Дата и время отправки: </span>
                        <?= esc($reis['date_otpr']); ?>
                        <?= esc($reis['time_otpr']); ?>
                    </p>
                    <p class="card-text">
                        <span class="d-block">Дата и время прибытия: </span>
                        <?= esc($reis['date_prib']); ?>
                        <?= esc($reis['time_prib']); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php else : ?>
    <p>Маршрут не найден.</p>
<?php endif ?>
</div>
<?= $this->endSection() ?>
