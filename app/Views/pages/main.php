<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div style="padding: 90px 0;">
    <div class="container"
         style="background:url('<?= base_url() ?>/img/map.svg') right top no-repeat; background-size: contain;">
        <div class="row">
            <div class="col-6" style="color: white; display:flex; flex-direction: column; justify-content: center;">
                <h1 class="mb-4" style="font-size: 54px;">ТрансАвто</h1>
                <div class="mb-4" style="padding-right: 50px;font-size: 22px;">
                    Транспортные грузоперевозки в любую точку России
                </div>
                <?php use IonAuth\Libraries\IonAuth;
                $ionAuth = new IonAuth();
                ?>
                <?php if (!$ionAuth->loggedIn()): ?>
                    <div>
                        <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-6">
                <img src="<?= base_url() ?>/img/car-on-map.svg" alt="" class="w-100">
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

