<?php

namespace App\Controllers;

use App\Models\MarshrytModel;
use App\Models\ReisModel;
use App\Models\TransportModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Reis extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReisModel();
        $data ['reis'] = $model->getReis();
        echo view('reis/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReisModel();
        $data ['reis'] = $model->getReis($id);
        echo view('reis/view', $this->withIon($data));
    }

    public function viewAllWithMarshryt()
    {
        if ($this->ionAuth->isAdmin()) {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            $data['per_page'] = $per_page;
            helper(['form', 'url']);
            $model = new ReisModel();
            $data['reis'] = $model->getReisWithMarshryt(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('reis/view_all_with_marshryt', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('60571KDN.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $model = new TransportModel();
        $data ['transport'] = $model->getTransport();
        $model = new MarshrytModel();
        $data ['marshryt'] = $model->getMarshryt();
        echo view('reis/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_marshryt' => 'required',
                'id_transport' => 'required',
                'date_otpr' => 'required',
                'time_otpr' => 'required',
                'date_prib' => 'required',
                'time_prib' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new ReisModel();
            $data = [
                'id_marshryt' => $this->request->getPost('id_marshryt'),
                'id_transport' => $this->request->getPost('id_transport'),
                'date_otpr' => $this->request->getPost('date_otpr'),
                'time_otpr' => $this->request->getPost('time_otpr'),
                'date_prib' => $this->request->getPost('date_prib'),
                'time_prib' => $this->request->getPost('time_prib'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('60571KDN.reis_create_success'));
            return redirect()->to('/reis');
        } else {
            return redirect()->to('/reis/create')->withInput();
        }
    }

    public
    function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReisModel();

        helper(['form']);
        $data ['reis'] = $model->getReis($id);
        $data ['validation'] = \Config\Services::validation();
        $model = new TransportModel();
        $data ['transport'] = $model->getTransport();
        $model = new MarshrytModel();
        $data ['marshryt'] = $model->getMarshryt();
        echo view('reis/edit', $this->withIon($data));
    }

    public
    function update()
    {
        helper(['form', 'url']);
        echo '/reis/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'id_marshryt' => 'required',
                'id_transport' => 'required',
                'date_otpr' => 'required',
                'time_otpr' => 'required',
                'date_prib' => 'required',
                'time_prib' => 'required',
                'picture_url' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new ReisModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'id_marshryt' => $this->request->getPost('id_marshryt'),
                'id_transport' => $this->request->getPost('id_transport'),
                'date_otpr' => $this->request->getPost('date_otpr'),
                'time_otpr' => $this->request->getPost('time_otpr'),
                'date_prib' => $this->request->getPost('date_prib'),
                'time_prib' => $this->request->getPost('time_prib'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/reis');
        } else {
            return redirect()->to('/reis/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public
    function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReisModel();
        $model->delete($id);
        return redirect()->to('/reis');
    }
}
