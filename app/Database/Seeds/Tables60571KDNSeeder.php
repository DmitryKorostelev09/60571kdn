<?php


namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Tables60571KDNSeeder extends Seeder
{
    public function run()
    {
        $data = [
            'naimen' => 'Сургут - Москва',
            'tarif' => '300',
            'rasstoyanie' => 1245,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Питер - Москва',
            'tarif' => '450',
            'rasstoyanie' => 705,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Питер - Уфа',
            'tarif' => '1000',
            'rasstoyanie' => 2505,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Сыктывкар - Уфа',
            'tarif' => '800',
            'rasstoyanie' => 405,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Калининград - Уфа',
            'tarif' => '140',
            'rasstoyanie' => 2205,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Калининград - Новосибирск',
            'tarif' => '710',
            'rasstoyanie' => 3800,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Казань - Тюмень',
            'tarif' => '200',
            'rasstoyanie' => 8505,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimen' => 'Сургут - Тюмень',
            'tarif' => '500',
            'rasstoyanie' => 2505,
        ];
        $this->db->table('marshryt')->insert($data);
        $data = [
            'naimenovanie' => 'Хендай ХД78',
            'gruzopodem' => 3000,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'Хино 300 серии (Дутро)',
            'gruzopodem' => 4000,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'Фотон Ауман БЖ11',
            'gruzopodem' => 12000,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'ГАЗ Некст4.6',
            'gruzopodem' => 900,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'ГАЗ Некст4.6',
            'gruzopodem' => 900,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'Скания 4 серии',
            'gruzopodem' => 10000,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'КамАЗ 65117',
            'gruzopodem' => 15000,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'naimenovanie' => 'ЗИЛ 4333',
            'gruzopodem' => 1500,
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'id_marshryt' => 1,
            'id_transport' => 1,
            'date_and_time_otpr' => '2021-04-22 10:00:00',
            'date_and_time_prib' => '2021-04-25 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 2,
            'id_transport' => 2,
            'date_and_time_otpr' => '2021-04-23 10:00:00',
            'date_and_time_prib' => '2021-04-26 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 3,
            'id_transport' => 3,
            'date_and_time_otpr' => '2021-04-24 10:00:00',
            'date_and_time_prib' => '2021-04-27 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 4,
            'id_transport' => 4,
            'date_and_time_otpr' => '2021-04-25 10:00:00',
            'date_and_time_prib' => '2021-04-28 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 5,
            'id_transport' => 5,
            'date_and_time_otpr' => '2021-04-26 10:00:00',
            'date_and_time_prib' => '2021-04-29 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 6,
            'id_transport' => 6,
            'date_and_time_otpr' => '2021-04-01 10:00:00',
            'date_and_time_prib' => '2021-04-10 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 7,
            'id_transport' => 7,
            'date_and_time_otpr' => '2021-04-07 10:00:00',
            'date_and_time_prib' => '2021-04-11 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
        $data = [
            'id_marshryt' => 8,
            'id_transport' => 8,
            'date_and_time_otpr' => '2021-04-08 10:00:00',
            'date_and_time_prib' => '2021-04-12 21:30:00',
        ];
        $this->db->table('reis')->insert($data);
    }
}