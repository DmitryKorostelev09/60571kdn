<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Create60571kdntables extends Migration
{
    public function up()
    {
        // Drop table 'marshryt' if it exists
        $this->forge->dropTable('marshryt', true);
        // Table structure for table 'marshryt'
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'naimen' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'tarif' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'rasstoyanie' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('marshryt');

        // Drop table 'transport' if it exists
        $this->forge->dropTable('transport', true);
        // Table structure for table 'transport'
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'naimenovanie' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'gruzopodem' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('transport');

        // Drop table 'reis' if it exists
        $this->forge->dropTable('reis', true);
        // Table structure for table 'reis'
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'id_marshryt' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
            'id_transport' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
            'date_and_time_otpr' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'date_and_time_prib' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('id_marshryt', 'marshryt', 'id', 'RESTRICT', 'RESTRICT');
        $this->forge->addForeignKey('id_transport', 'transport', 'id', 'RESTRICT', 'RESTRICT');
        $this->forge->createTable('reis');
    }

    public function down()
    {
        $this->forge->dropTable('marshryt', true);
        $this->forge->dropTable('transport', true);
        $this->forge->dropTable('reis', true);
    }
}
