<?php


namespace App\Models;

use CodeIgniter\Model;

class MarshrytModel extends Model
{
    protected $table = 'marshryt'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['naimen'];

    public function getMarshryt($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}