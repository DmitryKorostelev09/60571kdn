<?php


namespace App\Models;

use CodeIgniter\Model;

class ReisModel extends Model
{
    protected $table = 'reis'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id_marshryt', 'id_transport', 'date_otpr','time_otpr', 'date_prib', 'time_prib', 'picture_url'];

    public function getReis($id = null)
    {
        if (!isset($id)) {
            return $this->select('*, reis.id')
                ->join('marshryt', 'marshryt.id = reis.id_marshryt')
                ->join('transport', 'transport.id = reis.id_transport')
                ->findAll();
        }
        return $this->select('*, reis.id')
            ->join('marshryt', 'marshryt.id = reis.id_marshryt')
            ->join('transport', 'transport.id = reis.id_transport')
            ->where(['reis.id' => $id])->first();
    }

    public function getReisWithMarshryt($id = null, $search = '')
    {
        $builder = $this->select('*, reis.id')
            ->join('marshryt', 'marshryt.id = reis.id_marshryt')
            ->like('tarif', $search, 'both', null, true)
            ->orlike('naimen', $search, 'both', null, true);
        if (!is_null($id)) {
            return $builder->where(['reis.id' => $id])->first();
        }
        return $builder;
    }
}