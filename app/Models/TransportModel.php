<?php


namespace App\Models;

use CodeIgniter\Model;

class TransportModel extends Model
{
    protected $table = 'transport'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['naimenovanie'];

    public function getTransport($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}