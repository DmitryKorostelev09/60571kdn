<?php
return[
    'admin_permission_needed' => 'Вы не можете просматривать эту страницу, нужны права администратора.',
    'reis_create_success' => 'Маршрут успешно создан.',
    'login_with_google' => 'Войти через Google',
];