<?php


namespace App\Services;


use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('1079687390253-f8kquomdch4qi369m1m16pcn47nu3145.apps.googleusercontent.com');
        $this->google_client->setClientSecret('LOioHmCmCaBIi0J3wgHBlCzY');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }
}