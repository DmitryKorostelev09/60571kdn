-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Апр 22 2021 г., 16:19
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571KDN`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Gryz`
--

CREATE TABLE `Gryz` (
  `id` int NOT NULL COMMENT 'id груза',
  `id_Reis` int DEFAULT NULL COMMENT 'id рейса',
  `ves` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'вес в кг',
  `otpravitell` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'отправитель'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Gryz`
--

INSERT INTO `Gryz` (`id`, `id_Reis`, `ves`, `otpravitell`) VALUES
(1, 1, '127', 'Коделин И.Н.'),
(2, 2, '300', 'Абрамов И.И.'),
(3, 3, '600', 'Украинец А.Г.'),
(4, 4, '437', 'Заломов М.К.'),
(5, 5, '235', 'Кожевник Н.Н'),
(6, 6, '100', 'Унлеев У.Г.'),
(7, 7, '500', 'Бугагашевич Д.Н.'),
(8, 8, '347', 'Крысин И.А.'),
(9, 9, '333', 'Иванов И.И.'),
(10, 10, '432', 'Шматко А.Д.');

-- --------------------------------------------------------

--
-- Структура таблицы `Reis`
--

CREATE TABLE `Marshryt` (
  `id` int NOT NULL COMMENT 'id маршрута',
  `naimen(city-city)` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'наименование город-город',
  `tarif za 1 kg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'тариф за 1кг в рублях',
  `rasstoyanie` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'расстояние в км'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Reis`
--

INSERT INTO `Marshryt` (`id`, `naimen(city-city)`, `tarif za 1 kg`, `rasstoyanie`) VALUES
(1, 'Москва-Сургут', '300', '2136'),
(2, 'Москва-Сочи', '89', '1361'),
(3, 'Питер-Москва', '280', '634'),
(4, 'Питер-Челябинск', '150', '1909'),
(5, 'Уфа-Радужный', '1000', '1183'),
(6, 'Тюмень-Красноярск', '400', '1666'),
(7, 'Тюмень-Краснодар', '650', '2270'),
(8, 'Москва-Уфа', '370', '1164'),
(9, 'Питер-Тюмень', '450', '2042'),
(10, 'Туймазы-Краснодар', '820', '1836');

-- --------------------------------------------------------

--
-- Структура таблицы `Reis`
--

CREATE TABLE `Reis` (
  `id` int NOT NULL COMMENT 'id рейса',
  `id_Marshryt` int DEFAULT NULL COMMENT 'id маршрута',
  `id_Transport` int DEFAULT NULL COMMENT 'id транспорта',
  `date and time OTPR` datetime(6) NOT NULL COMMENT 'дата и время отправления',
  `date and time PRIB` datetime(6) NOT NULL COMMENT 'дата и время прибытия'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Reis`
--

INSERT INTO `Reis` (`id`, `id_Marshryt`, `id_Transport`, `date and time OTPR`, `date and time PRIB`) VALUES
(1, 1, 1, '2021-04-22 10:00:00.000000', '2021-04-25 21:30:00.000000'),
(2, 2, 2, '2021-04-27 06:00:00.000000', '2021-04-28 14:10:00.000000'),
(3, 3, 3, '2021-04-22 08:00:00.000000', '2021-04-22 23:00:00.000000'),
(4, 4, 4, '2021-05-01 10:00:00.000000', '2021-05-04 05:00:00.000000'),
(5, 5, 5, '2021-05-09 12:30:00.000000', '2021-05-11 10:30:00.000000'),
(6, 6, 6, '2021-05-04 14:30:00.000000', '2021-05-06 18:20:00.000000'),
(7, 7, 7, '2021-04-07 10:38:00.000000', '2021-04-10 12:14:00.000000'),
(8, 8, 1, '2021-04-26 16:00:00.000000', '2021-04-29 04:00:00.000000'),
(9, 9, 2, '2021-04-29 07:40:00.000000', '2021-04-30 21:50:00.000000'),
(10, 10, 3, '2021-04-11 20:22:00.000000', '2021-04-24 16:53:00.000000');

-- --------------------------------------------------------

--
-- Структура таблицы `Transport`
--

CREATE TABLE `Transport` (
  `id` int NOT NULL COMMENT 'id транспорта',
  `naimenovanie` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'наименование',
  `gruzopodem` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'грузоподъемность в кг'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Transport`
--

INSERT INTO `Transport` (`id`, `naimenovanie`, `gruzopodem`) VALUES
(1, 'Хендай ХД78', '3000'),
(2, 'Хино 300 серии (Дутро)', '4000'),
(3, 'Фотон Ауман БЖ11', '12000'),
(4, 'ГАЗ Некст4.6', '900'),
(5, 'Скания 4 серии', '10000'),
(6, 'КамАЗ 65117', '15000'),
(7, 'ЗИЛ 4333', '1500');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Gryz`
--
ALTER TABLE `Gryz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_Reis` (`id_Reis`);

--
-- Индексы таблицы `Reis`
--
ALTER TABLE `Marshryt`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Reis`
--
ALTER TABLE `Reis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_Transport` (`id_Transport`),
  ADD KEY `id_Marshryt` (`id_Marshryt`);

--
-- Индексы таблицы `Transport`
--
ALTER TABLE `Transport`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Gryz`
--
ALTER TABLE `Gryz`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id груза', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `Reis`
--
ALTER TABLE `Marshryt`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id маршрута', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `Reis`
--
ALTER TABLE `Reis`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id рейса', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `Transport`
--
ALTER TABLE `Transport`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id транспорта', AUTO_INCREMENT=8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Gryz`
--
ALTER TABLE `Gryz`
  ADD CONSTRAINT `Gryz_ibfk_1` FOREIGN KEY (`id_Reis`) REFERENCES `Reis` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Reis`
--
ALTER TABLE `Reis`
  ADD CONSTRAINT `Reis_ibfk_1` FOREIGN KEY (`id_Transport`) REFERENCES `Transport` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Reis_ibfk_2` FOREIGN KEY (`id_Marshryt`) REFERENCES `Marshryt` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
